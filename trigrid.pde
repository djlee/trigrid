color BG = #FFFFFF;
color BORDER = #F0F0F0;
color[] COLORS = { BG, #FFCC66, #33CC33, #0066CC };
float ROOT3 = sqrt(3);
float SIDE = 50;
float WIDTH = SIDE * ROOT3 / 2;

// indexed by x then y
int[][] grid;

void setup() {
  size(800, 600);

  int numColumns = (int) floor(width / WIDTH);
  int columnHeight = (int) (2 * (height / SIDE + 1));
  grid = new int[numColumns][];
  for (int x = 0; x < grid.length; x++) {
    grid[x] = new int[columnHeight];
  }
  background(BG);
  noLoop();
}

void draw() {
  if (needsReset) {
    for (int x = 0; x < grid.length; x++) {
      for (int y = 0; y < grid[x].length; y++) {
        grid[x][y] = 0;
      }
    }
    needsReset = false;
  }
  drawTriangleGrid();
}

void drawTriangleGrid() {
  int numColumns = floor(width / WIDTH);
  for (int column = 0; column < numColumns; column++) {
    drawTriangleColumn(column);
  }
}

void drawTriangleColumn(int column) {
  boolean oddColumn = column % 2 == 1;

  beginShape(TRIANGLE_STRIP);
  float nearX = column * WIDTH;
  float farX = (column + 1) * WIDTH;
  float y = -SIDE / 2
  vertex(oddColumn ? nearX : farX, y);

  for (int row = 0; y <= height; y += SIDE, row += 2) {
    vertex(oddColumn ? farX : nearX, y + SIDE / 2);

    prepareColors(column, row);
    vertex(oddColumn ? nearX : farX, y + SIDE);

    prepareColors(column, row + 1);
  }
  endShape();
}

void prepareColors(int column, int row) {
  int c = grid[column][row];
  fill(COLORS[c]);
  stroke(c == 0 ? BORDER : COLORS[c]);
}

void mousePressed() {
  toggleTriangle(mouseX, mouseY);
}

void toggleTriangle(int x, int y) {
  int column = floor(x / WIDTH);

  // Number of 'rows' k_r from this point to the upper right edge.
  // These rows are parallel to the lines y = x / ROOT3 + k_r * SIDE
  int rightRowCount = floor((y - x / ROOT3) / SIDE);

  // Number of 'rows' k_l from this point to the upper left edge.
  // These rows are parallel to the lines y = -x / ROOT3 + k_l * SIDE
  int leftRowCount = floor((y + x / ROOT3) / SIDE);

  int row;
  if (column + rightRowCount == leftRowCount) {
    // Right-pointing triangle
    row = rightRowCount * 2 + column + 1;
  } else {
    // Left-pointing triangle
    row = leftRowCount * 2 - column;
  }

  int currentIndex = grid[column][row];
  grid[column][row] = (currentIndex + 1) % COLORS.length;
  redraw();
}
